import pytest
import sys

from knopt import arg, run, main

@pytest.fixture
def set_args(monkeypatch):
    def f(*args, progname=sys.argv[0]):
        monkeypatch.setattr(sys, 'argv', [progname, *args])
    return f


def ex_positional(first, second):
    return first + ' ' + second

def ex_optional(first="hello", second="world"):
    return first + ' ' + second

def ex_snake(config_file='~/.config'):
    return config_file

def ex_snake_positional(test_arg):
    return test_arg

def ex_cast(foo: arg(type=int, cast=True) = 0):
    return foo

def ex_metavar(foo: arg(metavar='BAR')=None):
    return foo

def ex_switch(verbose=False):
    return verbose

class Program:
    def foo(self, first='hello'):
        return first

def test_positional_help(set_args, capsys):
    with pytest.raises(SystemExit) as exc:
        set_args('--help')
        run(ex_positional)

    assert exc.value.code == 0

    cap = capsys.readouterr()
    msg = cap.out.splitlines()
    assert msg[0].startswith('usage:')

    assert msg[2].startswith('positional arguments:')
    assert msg[3].strip() == 'first'
    assert msg[4].strip() == 'second'

    assert not cap.err

def test_positional(set_args):
    set_args('hello', 'world')
    value = run(ex_positional)
    assert value == "hello world"

def test_main(set_args, capsys):
    set_args('hello', 'world')
    f = main(ex_positional)

    assert not capsys.readouterr().out
    assert callable(f)
    assert f('foo', 'bar') == "foo bar"
    assert f.main() == "hello world"

def test_optional_help(set_args, capsys):
    with pytest.raises(SystemExit) as exc:
        set_args('--help')
        run(ex_optional)

    assert exc.value.code == 0

    cap = capsys.readouterr()
    msg = cap.out.splitlines()
    assert msg[0].startswith('usage:')

    assert msg[2].startswith('optional arguments:')
    assert '--first' in msg[4].strip()
    assert '--second' in msg[5].strip()

    assert not cap.err

def test_program(set_args):
    set_args('foo', '--first=bar')
    result = run(Program)
    assert result == 'bar'

def test_snake_case(set_args):
    set_args('--config-file', 'foo.conf')
    result = run(ex_snake)
    assert result == 'foo.conf'

def test_snake_positional(set_args):
    set_args('foo')
    result = run(ex_snake_positional)
    assert result == 'foo'

def test_version(set_args, capsys):
    set_args('--version', progname='test')
    with pytest.raises(SystemExit):
        run(ex_positional, version="3.4.5")

    out = capsys.readouterr().out.strip()
    assert out == 'test 3.4.5'

def test_cast(set_args, capsys):
    set_args('--foo', '5')
    result = run(ex_cast)
    assert type(result) == int
    assert result == 5

def test_metavar(set_args, capsys):
    set_args('--help')
    with pytest.raises(SystemExit):
        result = run(ex_metavar)
    cap = capsys.readouterr()
    msg = cap.out.splitlines()
    assert msg[4].strip() == '--foo BAR'
    assert not cap.err

def test_switch(set_args, capsys):
    set_args('--verbose')
    result = run(ex_switch)
    assert result

def test_not_switch(set_args, capsys):
    set_args()
    result = run(ex_switch)
    assert not result
