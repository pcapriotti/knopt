from argparse import ArgumentParser
import inspect


def arg(*names, **kwargs):
    """Annotation to turn an argument into a command-line option.

    Arguments with a default are turned into options, while the others
    become positional command-line arguments.

    The variable name of the argument is used as the main name of the
    option, but other names can be specified as positional arguments
    of `arg`. Names are only meaningful for options.

    Keyword arguments are generally forwarded to the `add_argument`
    method of `ArgParser`. The following keyword arguments are
    supported, and have the same behaviour as in `argparse`:

     - `help`
     - `action`
     - `nargs`
     - `metavar`.

    Note that `arg` returns an anonymous type subclassing the type
    specified as the `type` keyword argument (`str` by default).

    The parsed argument is by default converted to an instance of the
    type returned by `arg`. To remove one layer of indirection in the
    type hierarchy, it is possible to specify the keyword argument
    `cast=True`, in which case the underlying type is used instead.
    """
    base = kwargs.get('type', str)
    return type(base.__name__, (base,),
                {**kwargs, 'names': names})

def main(f=None, **kwargs):
    """Decoration to turn a function or class into a command-line program.

    #### As a function decorator

    When used as a decorator on a function, it creates a function
    taking its inputs from the command line arguments passed to the
    program, suitable to be used as a program main.

    If the decorated function resides at the top level, the resulting
    function is automatically run, otherwise it is stored in the
    attribute `main` of the underlying function.

    Every argument of the function is converted to an option
    (if it has a default) or a positional argument (if it does not).

    Arguments can be annotated with their types. The types are used to
    convert the parsed arguments before being passed to the underlying
    function.

    Moreover, arguments can be annotated with an `arg` type. This
    makes it possible to have further control on how an argument is
    turned into an option.

    The docstring of the function can be used to provide a description
    for the program, which is displayed together with the help text
    (i.e. when the program is invoked with the `--help` options).

    #### As a class decorator

    When used as a decorator on a class, it also creates a function to
    be used as a program main, but this time the resulting program
    accepts multiple commands (i.e. it contains subparsers, in
    `argparse` terminology).

    Every (non-special) method of the class is converted into a
    command, with options created from its arguments as above. The
    `__init__` method is treated specially, and its arguments are
    turned into global options.

    The docstring of a method is used as the description of the
    corresponding command, and it is displayed in the list of
    commands, as well as in its help text (displayed when giving the
    option `--help` after the command).
    """
    stack = inspect.stack()[1]
    mod = inspect.getmodule(stack[0])

    def decorator(f):
        if mod.__name__ == '__main__':
            run(f, **kwargs)
        else:
            f.main = lambda: run(f, **kwargs)
        return f

    if f is None: return decorator
    return decorator(f)

def run(f, **kwargs):
    """Run a function or class as a command-line program.

    This is the main workhorse of knopt's functionality, turning a
    function or a class into a command-line program. In most cases,
    there is no need to call this function directly, as it is
    automatically invoked by `main`.

    """
    version = kwargs.get('version')
    default_method = kwargs.get('default')

    parser_kwargs = {}
    if f.__doc__ is not None:
        parser_kwargs['description'] = f.__doc__
    parser = ArgumentParser(**parser_kwargs)
    if version is not None:
        parser.add_argument('--version', action='version',
                            version=f'%(prog)s {version}')
    if isinstance(f, type):
        init = getattr(f, '__init__')
        init_doc = "List of commands"
        if '__init__' in f.__dict__:
            _setup_parser(parser, init, method=True)
            if init.__doc__ is not None:
                init_doc = init.__doc__
        subs = parser.add_subparsers(title=init_doc)

        for method in dir(f):
            if method.startswith('_'): continue

            fun = getattr(f, method)
            sub = subs.add_parser(method, help=fun.__doc__)
            _setup_parser(sub, fun, method=True)
            sub.set_defaults(_run=fun)

        args = parser.parse_args()
        obj = f(*_build_args(init, args, method=True))

        if '_run' not in args:
            # no subparser selected, just run default
            if default_method is not None:
                fun = getattr(f, default_method)
                sub = subs.choices[fun.__name__]
                if '__init__' in f.__dict__:
                    _setup_parser(sub, init, method=True)
                args = sub.parse_args()
            else:
                raise Exception("no command selected")
        result = _call_with_args(args._run, args, self=obj)
        return result
    else:
        _setup_parser(parser, f)
        args = parser.parse_args()
        return _call_with_args(f, args)

def _arg_name(a, positional):
    if not positional:
        a = a.replace('_', '-')
        if len(a) > 1:
            a = '--' + a
        else:
            a = '-' + a
    return a

def _setup_parser(parser, f, method=False):
    if f.__doc__ is not None and parser.description is None:
        parser.description = f.__doc__
    spec = inspect.signature(f).parameters
    args = list(spec.keys())
    if method: args = args[1:]
    for a in args:
        params = [a]
        kwparams = {}
        ann = spec[a].annotation
        ty = None
        if ann != inspect._empty:
            if getattr(ann, 'cast', False):
                ty = ann.type
            else:
                ty = ann
            alts = getattr(ann, 'names', [])
            for alt in alts:
                params.append(alt)
            if hasattr(ann, 'help'):
                kwparams['help'] = getattr(ann, 'help')
            if hasattr(ann, 'action'):
                kwparams['action'] = getattr(ann, 'action')
            if hasattr(ann, 'metavar'):
                kwparams['metavar'] = getattr(ann, 'metavar')
            if hasattr(ann, 'nargs'):
                kwparams['nargs'] = getattr(ann, 'nargs')
        default = spec[a].default
        positional = default == inspect._empty
        if not positional:
            kwparams['default'] = default
            if ty is None:
                ty = type(default)
            if ty is bool or type(default) is bool:
              kwparams['action'] = 'store_true'
              ty = None
        if ty is not None:
            kwparams['type'] = ty
        params = map(lambda a: _arg_name(a, positional), params)

        parser.add_argument(*params, **kwparams)

def _build_args(f, args, method=False):
    spec = inspect.getfullargspec(f)
    f_args = spec.args
    if method:
        f_args = f_args[1:]
    for a in f_args:
        yield getattr(args, a)

def _call_with_args(f, args, self=None):
    if self is None:
        return f(*_build_args(f, args))
    else:
        return f(self, *_build_args(f, args, method=True))
