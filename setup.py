import os
import setuptools

APP_ROOT = os.path.dirname(__file__)
README = os.path.join(APP_ROOT, 'README.md')

setuptools.setup(
    name='knopt',
    version='0.3.99',
    description="Automatic command line apps",
    long_description=open(README).read(),
    long_description_content_type='text/markdown',
    url="https://gitlab.com/pcapriotti/knopt",
    author="Paolo Capriotti",
    author_email="paolo@capriotti.io",
    license="BSD3",
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    packages=["knopt"]
)
