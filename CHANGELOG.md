## Version 0.3

- Add simple switches

- Improve subparser help

## Version 0.2.1

- Fix package description

## Version 0.2

- Convert option names to kebab-case

- Add cast feature

- Add metavariable support

- Add nargs support

## Version 0.1

- Initial release
